package noahp78.survive.server;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import noahp78.survive.networking.NetworkingUtil;
import noahp78.survive.networking.requests.authRequest;
import noahp78.survive.networking.requests.gamedataRequest;
import noahp78.survive.networking.requests.initRequest;
import noahp78.survive.networking.requests.moveRequest;
import noahp78.survive.networking.responses.ChunkResponse;
import noahp78.survive.networking.responses.authResponse;
import noahp78.survive.networking.responses.gamedataResponse;
import noahp78.survive.networking.responses.initResponse;
import noahp78.survive.networking.responses.moveResponse;
import noahp78.survive.networking.responses.playerJoinEvent;
import noahp78.survive.networking.responses.timeResponse;
import noahp78.survive.plugin.EventPublisher;
import noahp78.survive.plugin.events.TickEvent;
import noahp78.survive.server.generators.WorldGenV2;
import noahp78.survive.server.managers.ServerSidePlayer;
import noahp78.survive.server.tick.TickHandler;
import noahp78.survive.util.ConfigHelper;
import noahp78.survive.util.ExceptionHelper;
import noahp78.survive.util.authUtils;
import noahp78.survive.util.logManager;
import noahp78.survive.util.map.World;
import noahp78.survive.util.map.io.Chunk;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class serverStart {
	private static int PlayerCount = 0;
	private static int TickCount = 0;
	private static String serverVersion = "Dev1";
	private static Server server;
	public static World serverWorld = new World();
	
	private static HashMap<Integer, ServerSidePlayer> playerMap = new HashMap<Integer, ServerSidePlayer>();
	private static HashMap<Integer, Long> TickTimes = new HashMap<Integer, Long>();
	private static Long InitTime;
	private static Long FinalTime;
	private static Long TickTimeStart;
	private static Long TickTimeEnd;
	private static int SpawnX;
	private static int SpawnY;
	
	
	
	public static void StartServer(int Port){
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHelper());
		InitTime = System.nanoTime();
		if ((new File("server.properties").isFile())){
			ConfigHelper.LoadServerConfig();
		}else{
			ConfigHelper.MakeServerConfig();
			ConfigHelper.LoadServerConfig();
		}
		logManager.LogInfo("Server starting on port: " + Port + "And UDP port " + (Port+1));
		logManager.LogInfo("Generating World");
		logManager.LogInfo("This server is running the OLD generation method. Mods FILES don't work here yet!");
	
		Port=51213;
		WorldGenV2.GenerateSpawn();
			
		logManager.LogInfo("World Generation Complete...");
		logManager.LogInfo("Starting ServerSide Tick Handler");
		Timer timer = new Timer();
		timer.schedule( new TimerTask() {
		    public void run() {
		    	Thread.setDefaultUncaughtExceptionHandler(new ExceptionHelper());
		    	TickTimeStart = System.nanoTime();
		    	TickCount++;
		    	TickHandler.Tick();
		    	EventPublisher.raiseEvent(new TickEvent());
		    	TickTimeEnd = System.nanoTime();
		    	if((TickTimeEnd-TickTimeStart)/ 1000000 > 99){
		    		logManager.LogWarning("Tick " + TickCount + " took " + (TickTimeEnd-TickTimeStart)/ 1000000 + "MS; Is the system overloaded?");
		    	}
		    	TickTimes.put(TickCount, (TickTimeEnd-TickTimeStart)/ 1000000);
		    }//  V normal is 100
		 }, 0, 1*10);
		
		server = new Server(65536,65536);
		server.start();
		server.getUpdateThread().setUncaughtExceptionHandler(new ExceptionHelper());
		Kryo kryo = server.getKryo();
		NetworkingUtil.register(server);
		FinalTime = System.nanoTime();
		logManager.LogInfo("Server started in: " + (System.nanoTime()-InitTime)/ 1000000 + "MS");
		   
		try {
			server.bind(Port, (Port+1));
		} catch (IOException e) {
			System.out.println("Server can't bind to port");
			System.out.println(e.getCause());
		}
		    server.addListener(new Listener() {
		        public void received (Connection connection, Object object) {
		        	if (object instanceof initRequest) {
		        		//Basic initial connection stuff... 
		        		logManager.LogInfo("[ServerConnection]" + connection.getRemoteAddressTCP() + " Connected to the server");
		        		 initResponse response = new initResponse();
		        		 PlayerCount = server.getConnections().length;
		        		 response.mayConnect = true;
		        		 response.playercount = PlayerCount;
		        		 response.ServerVersion = serverVersion;
		        		 connection.sendTCP(response);
		        		 playerJoinEvent response2 = new playerJoinEvent();
		        		 response2.charID = PlayerCount;
		        		 logManager.LogDebug("[server] Sending LoginEvent to all players");
		        		 server.sendToAllTCP(response2); 
		        	}
		        	if (object instanceof authRequest) {
		        		logManager.LogDebug("Got AUTH Request");
		        		String username = ((authRequest) object).username;
		        		String token = ((authRequest) object).token;
		        		String IP = ((authRequest) object).ip;
		        		
		        		try {
							if (authUtils.CheckAuth(username, token, IP)==true){
								authResponse response = new authResponse();
								response.GoOn = true;
								response.error = "n.i.p";
								connection.sendTCP(response);
								PlayerCount = server.getConnections().length;
								ServerSidePlayer P = new ServerSidePlayer();
								P.tileX=0;
								P.tileZ=0;
								P.username=username;
								playerMap.put(PlayerCount, P);
								//And now, Notify ALL the players ingame
								
							}
						} catch (Exception e) {
							logManager.LogWarning("Auth servers might be down... Or you aren't connected to the internet");
							e.printStackTrace();
						}
		        	}
		        	if (object instanceof gamedataRequest){
		        		gamedataResponse response = new gamedataResponse();
		        		//response.Players = null;
		        		PlayerCount = server.getConnections().length;
		        		connection.sendTCP(response);
		        		//Start looping through all tiles. (Generates lots of server load, I know, TODO: Implement "chunk" based sending.
		        		int x = 0;
		        		int y = 0;
		        		 		while( x < 5 ) {
		        		 			while (y<5){
		        		 				String Combined = (x + ":" + y);
		        		 				ChunkResponse response4 = new ChunkResponse();
		        		 				response4.x=x*16;
		        		 				response4.y=y*16;
		        		 				Chunk tileChunk = serverWorld.worldContainer.LoadChunk(x*16,y*16);
		        		 				response4.chunk = tileChunk;
		        		 				connection.sendTCP(response4);	
		        		 				y++;
		        		 				
		        		 			}
		        		 			y=0;
		        		 			x++;
		        		 		}
		        		 		
		        	}
		        	if (object instanceof moveRequest){
		        		moveResponse response = new moveResponse();
		        		int CharID = ((moveRequest) object).CharID;
		        		int x = ((moveRequest) object).tileX;
		        		int z = ((moveRequest) object).tileZ;
		        		//logManager.LogDebug("[Server]Sending Movement Packet to all");
		        		response.CharID = CharID;
		        		response.x = x;
		        		response.y = z;
		        		server.sendToAllTCP(response);
		        	}
		        	
		        	
		        }
		        
		     });
		    EnterCommand();
	}
	private static void EnterCommand(){
		System.out.print("> ");
	}
	public static void SendnewTime(int time){
		timeResponse response = new timeResponse();
		response.newTime = time;
		
		if (PlayerCount < 1){
			//System.out.println("Nobody notified about new time, Nobody is connected!");
		}else{
			if (TickCount > 200){
				server.sendToAllTCP(response);
			}else{
			}
		
		}
	}
}
