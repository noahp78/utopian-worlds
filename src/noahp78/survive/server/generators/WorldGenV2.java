package noahp78.survive.server.generators;

import java.util.HashMap;

import java.util.Random;

import noahp78.survive.server.serverStart;
import noahp78.survive.server.managers.ServerSidePlayer;
import noahp78.survive.util.logManager;
import noahp78.survive.util.map.World;
import noahp78.survive.util.map.io.Container;
/** Infinite Terrain Generation based on Pelrin Noise!
 * 
 * @author noah
 *
 */

public class WorldGenV2 {
	public static HashMap<String, Integer>WorldMap = new HashMap<String,Integer>();
	public static HashMap<String, ServerSidePlayer>PlayerMap = new HashMap<String,ServerSidePlayer>();
	public static int y2;
	public static int x2;
	/** Generate a simple, infinte world
	 * 
	 */
	public static void GenerateSpawn(){
		ChunkGen.init();
	int x = 0;
	int y = 0;
		while( x < 5 ) {
			while (y<5){
				String Combined = (x*16 + ":" + y*16);
					// Generate the chunk
				logManager.LogDebug("Generating Chunk " + x*16 + ":" + y*16);
				ChunkGen.Generate(x*16, y*16);
				y++;
				
			}
			x++;
			y=0;
		}
	}
	/** Forces the world to generate arround the said point
	 *  (20*20 Field);
	 * @param X
	 * @param Y
	 */
	public static void GenerateArround(int X, int Y){
		
	}
	
}
