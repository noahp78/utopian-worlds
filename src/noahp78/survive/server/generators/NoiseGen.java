package noahp78.survive.server.generators;

import java.util.Random;

import org.matheusdev.interpolation.FloatInterpolation;
import org.matheusdev.interpolation.FloatInterpolationFunc;
import org.matheusdev.noises.noise2.SimplexNoise2;

public class NoiseGen {
	public static SimplexNoise2 test;
	public static void Init(){
        final FloatInterpolation interp = new FloatInterpolationFunc() {
            @Override
            protected float func(float t) {
                float t2 = t * t;
                return 3 * t2 - 2 * t2 * t;
            }
        };
        final Random rand = new Random();
		test = new SimplexNoise2(512, 512, 2, rand, interp);
	}
	public static int get(int x, int y) {

		
	    float noiseValue = test.get(x, y);
	    //System.out.println("Got Noise Value " + noiseValue);
	    if(noiseValue > 0) {
	        if(noiseValue <= 0.05f) {
	            return 4;
	        	//return TileType.SAND.id;
	        } else if(noiseValue <= 0.2f) {
	            return 1;
	        	//retur TileType.GRASS.id;
	        } else if(noiseValue <= 0.5) {
	            return 2;
	        	//return TileType.TREE.id;
	        } else if(noiseValue <= 0.7) {
	            return 3;
	        	//return TileType.DIRT.id;
	        } else {
	        	return 5;
	            //return TileType.STONE.id;
	        }
	    }
	    return 6;
	    //return TileType.WATER.id;
	}
}
