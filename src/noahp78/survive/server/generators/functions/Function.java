package noahp78.survive.server.generators.functions;

import noahp78.survive.util.map.io.Chunk;

public interface Function {
	/** This can force the generation of chunks and overwrite the data for the Function
	 * 
	 * @param X
	 * @param Y
	 */
	public void Generate(int X, int Y, Chunk c);
}
