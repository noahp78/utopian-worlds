package noahp78.survive.server.generators;

import java.util.HashMap;
import java.util.Random;

import noahp78.survive.server.managers.ServerSidePlayer;
import noahp78.survive.util.map.BasicTile;
import noahp78.survive.util.map.TileRegistry;
import noahp78.survive.util.map.World;

public class WorldGenV1 {
	public static HashMap<String, Integer>WorldMap = new HashMap<String,Integer>();
	public static HashMap<String, ServerSidePlayer>PlayerMap = new HashMap<String,ServerSidePlayer>();
	
	/** Generate a simple world. Only trees are spawned.
	 * 
	 */
	public static World GenerateWorld(){
	
	
	int x = 0;
	int y = 0;
		while( x < 100 ) {
			while (y<100){
				String Combined = (y + ":" + x);
				WorldMap.put(Combined,1);
				y++;
			}
			x++;
			y=0;
		}
	int treegen=0;
	while(treegen < 200){
	treegen++;
	Random randomGenerator = new Random();
	int randomX = randomGenerator.nextInt(100);
	int randomY = randomGenerator.nextInt(100);
	String Combined = (randomX + ":" + randomY);
	WorldMap.put(Combined,2);
	}
	
	World worldname = new World();
	//worldname.WorldMap = WorldMap;
	worldname.PlayerMaps = PlayerMap;
	return worldname;
	}
}

