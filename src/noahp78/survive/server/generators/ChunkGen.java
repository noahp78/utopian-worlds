package noahp78.survive.server.generators;

import noahp78.survive.server.serverStart;
import noahp78.survive.util.logManager;
import noahp78.survive.util.map.io.Chunk;

/** Generates a chunk.
 * 
 * @author noah
 *
 */
public class ChunkGen {
	public static void init(){
		NoiseGen.Init();
	}
	public static void Generate(int X, int Y){
		/* Check Chunk left, up and right for specific functions. But don't try to generate
		 *  them if they don't exist
		 */
		
		Chunk newChunk = new Chunk();
		int x2 = 0;
		int y2 = 0;
		while (x2<17){
			while (y2<17){
				int TileID = NoiseGen.get(x2+X, y2+Y);
					newChunk.AddToMap(x2, y2, TileID);

				//System.out.println("WORLDGEN: GENERATED " + x2 + " And X= " + X + ":" + y2+ " AND Y IS: " +Y);
				y2++;
				
			}
			x2++;
			y2=0;
		}
		x2=0;
		// We are done. safe to container
		serverStart.serverWorld.worldContainer.WriteChunk(newChunk,X,Y);
		
	}
	
}
