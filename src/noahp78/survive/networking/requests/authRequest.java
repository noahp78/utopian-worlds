package noahp78.survive.networking.requests;
/** Auth information to send to the server
 * 
 * @author noahp78
 *
 */
public class authRequest {
	public String username;
	public String token;
	public String ip;
	
}
