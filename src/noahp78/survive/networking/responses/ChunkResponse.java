package noahp78.survive.networking.responses;

import noahp78.survive.util.map.io.Chunk;

public class ChunkResponse {
	public Chunk chunk;
	public int x;
	public int y;
}
