package noahp78.survive.networking.responses;

/** 
 * 
 * @author noah
 * @params
 * tile = BasicTile used (use TileRegistry.GetTile(id) to find the tile with the ID)
 * Cords = Cordinates of the Tile (In format X:Y)
 * 
 */
public class TileDataResponse {
	public Integer tileID;
	public String Cords;
	
}
