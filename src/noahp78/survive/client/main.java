package noahp78.survive.client;

/* this is our client side. Server side will be implented when we get MP support
 * 
 * 
 */
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import noahp78.survive.client.networking.mainClient;
//import noahp78.survive.client.render.Camera;
import noahp78.survive.client.render.ConnectionScreen;
import noahp78.survive.client.render.DebugScreen;
import noahp78.survive.client.render.LogoRender;
import noahp78.survive.client.render.MainMenu;
import noahp78.survive.client.render.MultiplayerRender;
import noahp78.survive.client.render.WorldRender;
import noahp78.survive.client.render.chatRender;
import noahp78.survive.client.render.loginMenu;
import noahp78.survive.client.render.timeRender;
import noahp78.survive.plugin.EventPublisher;
import noahp78.survive.plugin.events.ClientInitEvent;
import noahp78.survive.plugin.loader.pluginLoader;
import noahp78.survive.server.serverStart;
import noahp78.survive.util.ExceptionHelper;
import noahp78.survive.util.Reference;
import noahp78.survive.util.logManager;
import noahp78.survive.util.map.BasicTile;
import noahp78.survive.util.map.DefaultTileSet;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class main extends BasicGame {
	// 1 is main menu, 2 is connectionscreen, 3 = game, 4 = paused
	// 5 = login screen, 6= connect to server screen 7 = ChatOverlay 8 =
	// logoscreen
	public static int Menu = 8;

	public static int CharX = 0;
	public static int CharY = 0;
	// image Init
	public Image img;
	public Image woodIcon;

	/** True if space is down */
	private boolean left;
	/** True if left shift is down */
	private boolean right;
	/** True if right shift is down */
	private boolean up;
	private boolean down;
	private boolean enter;
	private boolean use;
	private static boolean click;

	private boolean enterswitch = false;
	private boolean Upswitch = false;
	public static int FrameCount = 1600;

	private boolean Downswitch = false;
	private boolean Leftswitch = false;
	private boolean Rightswitch = false;

	public static int charID;
	public static boolean connected = true;
	public static String token = "3WnO1dobsqa2TCK";
	public static String username = "noah";
	public static String ip = "0123";
	public static int MouseX = 0;
	public static int MouseY = 0;

	// public static Camera camera;
	public static String WorldArray[];
	public static MouseInputHandler mouse;

	public main(String gamename) {
		super(gamename);
	}

	// Create some base assets (Base:Map)

	// Rendering Hooks
	/**
	 * Init Game, Inits all our classes
	 * 
	 */
	@Override
	public void init(GameContainer gc) throws SlickException {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHelper());
		// Init MouseListener
		mouse = new MouseInputHandler();
		gc.getInput().addMouseListener(mouse);

		// Init Base map
		LogoRender.Init();
		RenderINV.init();
		try {
			MainMenu.Init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnectionScreen.Init();
		logManager.init();
		MultiplayerRender.init();
		loginMenu.Init(gc);
		chatRender.Init();
		DefaultTileSet.init();
		DebugScreen.init();
		// Give mods the ability to load.

		EventPublisher.raiseEvent(new ClientInitEvent());
		// serverStart.StartServer(25565);
		// mainClient.Connect("127.0.0.1", 25565);

		// grassMap = new TiledMap("assets/GrassMap.tmx");
		// camera = new Camera(gc, grassMap);
		img = new Image("assets/textures/Character.png");
		woodIcon = new Image("assets/textures/woodIcon.png");
		// Start Servre
	}

	/**
	 * Called before each render. Do Render here
	 * 
	 */
	@Override
	public void update(GameContainer gc, int i) throws SlickException {
		// Key input
		FrameCount++;
		if (FrameCount == 1500) {
			// logManager.LogDebug("doing WorldUpdate...");
			UpdateGrassMap();
			FrameCount = 0;

		}

		up = gc.getInput().isKeyDown(Input.KEY_UP);
		down = gc.getInput().isKeyDown(Input.KEY_DOWN);
		left = gc.getInput().isKeyDown(Input.KEY_LEFT);
		right = gc.getInput().isKeyDown(Input.KEY_RIGHT);
		use = gc.getInput().isKeyDown(Input.KEY_E);
		enter = gc.getInput().isKeyDown(Input.KEY_ENTER);

		if (Menu == 0) {
			// Only press once eatch frame (Stops crazy flying)
			if (up) {
				if (!(Upswitch)) {
					Upswitch = true;
					moveCharUp();

				}
				if ((Upswitch)) {

				}
			}
			if (!(up)) {
				Upswitch = false;
			}

			// Code, Sleep, Repeat
			if (down) {
				if (!(Downswitch)) {
					Downswitch = true;
					movecharDown();
				}
				if ((Downswitch)) {

				}
			}
			if (!(down)) {
				Downswitch = false;
			}
			// End of line

			if (left) {
				if (!(Leftswitch)) {
					Leftswitch = true;
					moveCharLeft();

				}
				if ((Leftswitch)) {

				}
			}
			if (!(left)) {
				Leftswitch = false;
			}
			if (right) {
				if (!(Rightswitch)) {
					Rightswitch = true;
					moveCharRight();

				}
				if ((Rightswitch)) {

				}
			}
			if (!(right)) {
				Rightswitch = false;
			}
		}
		if (Menu == 1) {
			if (up) {
				if (!(Upswitch)) {
					Upswitch = true;
				}
				if ((Upswitch)) {

				}
			}
			if (!(up)) {
				Upswitch = false;
			}
			if (down) {
				if (!(Downswitch)) {
					Downswitch = true;
					if ((Downswitch)) {

					}
				}
				if (!(down)) {
					Downswitch = false;
				}
			}
			if (enter) {
				if (!(enterswitch)) {
					enterswitch = true;

					Menu = 2;
					serverStart.StartServer(5244);
					mainClient.Connect("Localhost", 5244);

					// TODO remove server start code
				}
				if ((enterswitch)) {

				}

			}
			if (!(enterswitch)) {
				enterswitch = false;
			}
		}
		if (Menu == 5) {
			if (enter) {
				if (!(enterswitch)) {
					enterswitch = true;
					Menu = 1;
					// Now login to our server
					try {
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logManager.LogCrash(e.getStackTrace().toString());
					}

					if (token.equals("Error Logging in: Bad Login")) {
						Menu = 5;

					} else {
						// serverStart.StartServer(5244);
						// mainClient.Connect("Localhost", 5244);
					}
				}

				if ((enterswitch)) {

				}
				if (!(enterswitch)) {
					enterswitch = false;
				}
			}
		}
		if (Menu == 7) {
			if (enter) {
				if (!(enterswitch)) {
					enterswitch = true;

				}

				if ((enterswitch)) {
				}
				if (!(enterswitch)) {
					enterswitch = false;
				}
			}
		}
	}

	/**
	 * Render method
	 * 
	 */
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		if (Menu == 0) {
			use = gc.getInput().isKeyDown(Input.KEY_E);
			WorldRender.Render();
			img.draw(CharX, CharY);
			MultiplayerRender.Render(gc, g);
			RenderINV.render(gc, g);
			timeRender.render(gc, g);
			// V Breaks Everything! DOn't use! V
			// LightRender.UpdateLightMapAndRender(g);
			chatRender.render(gc, g);

		}
		if (Menu == 1) {
			MainMenu.render(gc, g);
		}
		if (Menu == 2) {
			ConnectionScreen.render(gc, g);
		}
		if (Menu == 5) {
			loginMenu.render(gc, g);
		}
		if (Menu == 8) {
			LogoRender.render(gc, g);
		}
		if (Reference.getDebug()) {
			DebugScreen.render(gc, g);

		}
	}

	/**
	 * Main Applet. Launches Client
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		logManager.LogInfo("[PluginSystem] Starting Init event of Plugins");
		pluginLoader.load();
		logManager.LogInfo("[PluginSystem] Started...");
		try {
			ip = "0123";

			int Port = 2154;
			System.out.println("Found IP for token: " + ip);
			AppGameContainer appgc;
			appgc = new AppGameContainer(new main("Survive!"));
			appgc.setDisplayMode(640, 480, false);
			appgc.setShowFPS(false);
			appgc.start();
			connected = false;

		} catch (SlickException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void moveCharUp() {
		if (!(CharY == 0)) {
			CharY = (CharY - 32);
			// ServerConnection.GetWorldData(CharX, CharY);
			moveChar(CharY, CharX);
		}

	}

	public static void movecharDown() {
		if (!(CharY == (32 * 100))) {
			CharY = (CharY + 32);
			System.out.println("Move to " + (CharY / 32));
			moveChar(CharY, CharX);
			// ServerConnection.GetWorldData(CharX, CharY);

		}

	}

	public static void moveCharLeft() {
		if (!(CharX == 0)) {
			CharX = (CharX - 32);
			System.out.println("Move to " + (CharX / 32));
			moveChar(CharY, CharX);
			// ServerConnection.GetWorldData(CharX, CharY);
		}
	}

	public static void moveCharRight() {
		if (!(CharX == 32 * 100)) {
			CharX = (CharX + 32);
			System.out.println("Move to " + (CharX / 32));
			moveChar(CharY, CharX);
			// ServerConnection.GetWorldData(CharX, CharY);
		}
	}

	public static void printMap(Map mp) {
		Iterator it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			System.out.println(pairs.getKey() + " = " + pairs.getValue());
			it.remove(); // avoids a ConcurrentModificationException
		}
	}

	public static void UpdateGrassMap() {
		int x5 = -1;
		int y5 = -1;
		while (x5 < 100) {
			x5++;
			while (y5 < 100) {
				y5++;
				String Combined = (y5 + ":" + x5);
				BasicTile ValueS = mainClient.TempWorldMap.get(Combined);
				if (ValueS != null) {
					WorldRender.AddTileToMap(ValueS, x5, y5);

				} else {
					// logManager.LogWarning("[main.UpdateGrassMap] TileLOCATON "
					// + Combined + " = null, Ignoring...");
				}
			}
			y5 = -1;
		}

	}

	public static void moveChar(int x, int y) {
		x = x / 32;
		y = y / 32;
		mainClient.MovePlayer(x, y);
	}

	// This is for mouse input, Yay :D

	@Override
	public void mouseClicked(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		MouseX = arg2;
		MouseY = arg3;
		// System.out.println("New Mouse X:" + arg2 + "New Mouse Y: " + arg3);

		// System.out.println("Mouse location is now: " + arg2 + ": " +arg3);

	}

	@Override
	public void mousePressed(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		System.out.println("Mouse PRessed!");
		click = true;

	}

	@Override
	public void mouseReleased(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		System.out.println("Mouse Released");
		click = false;
	}

	@Override
	public void mouseWheelMoved(int arg0) {
		// TODO Auto-generated method stub

	}

	public static Boolean MClick() {
		return click;
	}

	public static void startSinglePlayer() {
		serverStart.StartServer(25565);
		mainClient.Connect("127.0.0.1", 25565);
	}
}