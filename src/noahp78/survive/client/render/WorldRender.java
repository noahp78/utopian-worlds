package noahp78.survive.client.render;

import java.util.HashMap;
import java.util.logging.LogManager;

import noahp78.survive.client.main;
import noahp78.survive.util.logManager;
import noahp78.survive.util.map.BasicTile;
import noahp78.survive.util.map.TileRegistry;

import org.newdawn.slick.Image;
/** Responsible for the Multiplayer world (render and local storage)
 * 
 * @author noahp78
 *
 */
public class WorldRender {
	public static HashMap<String, BasicTile>WorldMap = new HashMap<String, BasicTile>();
	public static HashMap<Image, Integer>TileImage = new HashMap<Image, Integer>();
	/** Put's a tile on the giving position on the map
	 * 
	 * @param tile - BasicTile
	 * @param x TileCords
	 * @param y TileCords
	 * 
	 */
	public static void AddTileToMap(BasicTile tile, int x, int y){
		WorldMap.put(x + ":" + y, tile);
	}
	/** Init method, Transfers tile registry to our own registry.
	 * 
	 */
	public static void init(){
		//Hoping all mods are loaded now and reading all the tileIDS and their tilemaps
		int TotalTiles = TileRegistry.getTileCount();
		while (TotalTiles!=0){
			TileImage.put(TileRegistry.getTile(TotalTiles).tileImage, TotalTiles);
			TotalTiles--;
			
		}
	}
	/** Render the current world
	 * 
	 */
	public static void Render(){
		//Loop the loop
		int x1=0;
		int y1=0;
		while( x1 < 100 ) {
			while ( y1 < 100){
				String Combined = (x1 + ":" + y1);
				BasicTile tile2 = WorldMap.get(Combined);
				if (tile2!=null){
					//tile2.tileImage.draw(x1,y1);
					tile2.tileImage.draw(x1*32-main.CharX,y1*32-main.CharY);
				}else{
					//logManager.LogWarning("TileLocation: " + Combined + "= null, Ignoring...");
				}
				y1++;	
				}
			y1=0;
			x1++;	
			}
		}
		
	}
