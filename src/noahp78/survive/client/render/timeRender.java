package noahp78.survive.client.render;

import noahp78.survive.util.logManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
/** Renders the overlay responsible for light
 * 
 * NEEDS UPDATING:
 *  - Make Lighting system tile based (Make it that tiles have a light value)
 * 
 * @author noahp78
 *
 */
public class timeRender {
	public static int Time;
	private static float lightLevel = 0.0F;
	/** Renders the overlay
	 * 
	 * @param gc
	 * @param g
	 * @throws SlickException
	 */
	public static void render(GameContainer gc, Graphics g) throws SlickException {
		RecalculateLight();
		g.setColor(new Color(0.1f, 0.1F, 0.1f, lightLevel));
		g.fillRect(0, 0, 640, 800);
	}
	/** Time goes forward a bit
	 * 
	 */
	public static void TimeTick(){
		Time++;
	}
	/** Recalclate light levels for all tiles
	 * 
	 */
	public static void RecalculateLight(){
		if (Time < 680){
			//It's day!
			lightLevel = 0.0F;
		}else if (Time > 680){
			if (Time > 1000){
				// It's night
				lightLevel =0.9F;
				
			}else{
				lightLevel = (float) ((float)(Time-1000.0)*(1.0/120.0f));
			}
		}
		
		
		
	}
}
