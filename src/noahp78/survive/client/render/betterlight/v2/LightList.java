package noahp78.survive.client.render.betterlight.v2;

import java.util.ArrayList;
import java.util.Iterator;

public class LightList {

   // if only one light is allowed on one tile, then change to hash map and use x*y as the key. Is a problem when using dynamic light.
   private ArrayList<Light> lights = new ArrayList<Light>();
   
   public LightList(){
      
   }
   
   public void addLight(Light light){
      lights.add(light);
   }
   
   public Iterator<Light> getLights(){
      return(lights.iterator());
   }
   
   public boolean isEmpty(){
      return(lights.isEmpty());
   }
   
   public Light getLightAt(int x, int y){
      Light light = null;
      
      Iterator<Light> it = lights.iterator();
      while(it.hasNext()){
         light = it.next();
         if (light.getPosX() == x && light.getPosY() == y)
            return(light);
      }
      
      return(null);
   }
}