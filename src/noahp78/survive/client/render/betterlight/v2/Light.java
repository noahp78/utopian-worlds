package noahp78.survive.client.render.betterlight.v2;
import org.newdawn.slick.Color;

public class Light {
   private int xpos; // should be float to allow off center light
   private int ypos; // should be float to allow off center light
   private float strength; 
   private Color col;
   
   public Light(int x, int y, float str, Color col) {
      xpos = x;
      ypos = y;
      strength = str;
      this.col = col;
   }
   
   public void setLocation(int x, int y) {
      xpos = x;
      ypos = y;
   }

   public int getPosX() {
      return(xpos);
   }

   public int getPosY() {
      return(ypos);
   }

   // From slick forum, changed x and y to float to allow half tiles, also in preparation for pixel perfect lighting
   public float[] getEffectAt(float x, float y, boolean colouredLights) {
      float dx = (x - xpos);
      float dy = (y - ypos);
      float distance2 = (dx*dx)+(dy*dy);
      float effect = 1 - (distance2 / (strength*strength));
      
      if (effect < 0) {
         effect = 0;
      }
      
      if (colouredLights) {
         return new float[] {col.r * effect, col.g * effect, col.b * effect};
      } else {
         return new float[] {effect,effect,effect};
      }
   }
}