package noahp78.survive.client.render;

import noahp78.survive.client.main;
import noahp78.survive.util.Reference;
import noahp78.survive.util.map.BasicTile;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class DebugScreen {
	
	public static UnicodeFont DebugFont;
	/** Renders our F3 screen. (Gives information about the game)
	 *  @author noahp78
	 * @throws SlickException 
	 */
	public static void init() throws SlickException{
		String fontPath = "assets/fonts/kenvector.ttf";
		DebugFont = new UnicodeFont(fontPath, 12, true, false);
		DebugFont.addAsciiGlyphs();
		DebugFont.addGlyphs(400, 600);
		DebugFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		DebugFont.loadGlyphs();
	}
	public static void render(GameContainer gc, Graphics g) throws SlickException {
		DebugFont.drawString(1, 1, "Utopian Worlds V" + Reference.GetVersion());
		DebugFont.drawString(1, 13, "Loaded Plugins: " + Reference.GetLoadedPlugins());
			if (main.Menu==0){
			DebugFont.drawString(1, 25, "Currently rendering: WorldRender; MultiplayerRender"); 
			}
			if (main.Menu==1){
				DebugFont.drawString(1, 25, "Currently rendering: MainMenu"); 
			}
			if (main.Menu==2){
				DebugFont.drawString(1, 25, "Currently rendering: ConnectionScreen"); 
			}
			if (main.Menu==5){
				DebugFont.drawString(1, 25, "Currently rendering: LoginRender;"); 
			}
			if (main.Menu==8){
				DebugFont.drawString(1, 25, "Currently rendering: LogoRender;"); 
			}
		DebugFont.drawString(1, 38, "FPS: " +gc.getFPS());
		if (main.Menu==0){
			//We are in game and should also render some of that
			DebugFont.drawString(1, 51, "X/Z   " + main.CharX + " / " + main.CharY + " TileCords X/Z:   " + main.CharX/32 + " / " + main.CharY/32);
			int x1 = main.CharX/32;
			int y1 = main.CharY/32;
			String Combined = (x1 + ":" + y1);
			BasicTile tile2 = WorldRender.WorldMap.get(Combined);
			if (tile2!=null){
			DebugFont.drawString(1, 64, "Current Tile: " + tile2.techName + " AKA: " + tile2.Humanname);
			}
			DebugFont.drawString(1, 77, "Current Time: " + timeRender.Time);
			}
	}

}
