package noahp78.survive.client.render;

import noahp78.survive.client.main;
import noahp78.survive.client.sound.PlaySound;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

/** Renders the main menu
 * 
 * @author noahp78
 *
 */

public class MainMenu {
	private static Image buttonDisabled;
	private static Image buttonEnabled;
	private static Image background;
	public static UnicodeFont fpsFont;
	public static UnicodeFont BigFont;
	public static boolean firstFrame=true;
	private static int CurrentButton=0;
	
	/** Init the mainMenu, Call before render
	 * @throws Exception 
	 */
	public static void Init()throws Exception{
		PlaySound.Init();
		String fontPath = "assets/fonts/kenvector.ttf";
		fpsFont = new UnicodeFont(fontPath, 15, true, false);
		fpsFont.addAsciiGlyphs();
		fpsFont.addGlyphs(400, 600);
		fpsFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		fpsFont.loadGlyphs();
		BigFont = new UnicodeFont(fontPath, 25, true, false);
		BigFont.addAsciiGlyphs();
		BigFont.addGlyphs(400, 600);
		BigFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		BigFont.loadGlyphs();
		System.out.println("Preparing Main Menu");
		buttonDisabled = new Image("assets/textures/ui/blue_button00.png");
		buttonEnabled = new Image("assets/textures/ui/green_button00.png");
		//background = new Image("assets/textures/BackgroundTemp.png");
		
	}
	/** Renders the main menu.
	 * 
	 * @param gc
	 * @param g
	 * @throws SlickException
	 */
	public static void render(GameContainer gc, Graphics g) throws SlickException {
		if (firstFrame){
			firstFrame=false;
			PlaySound.PlayMenuBackground();
		}
		//g.setBackground(new Color(1.0f, 1.0F, 1.0f));
		g.setBackground(Color.orange);
		
		if (main.MouseX>225){
			if (main.MouseX<415){
			if (main.MouseY>100){
				if (main.MouseY<150){
					if(CurrentButton==0){
						CurrentButton=1;
						PlaySound.PlayRoll();
					}
					buttonEnabled.draw(225,100);
					if(main.MClick()){
						// We pressed SinglePlayer
						System.out.println("Starting SP");
						PlaySound.StopBackground();
						main.startSinglePlayer();
					}
					buttonDisabled.draw(225,200);
				}else if(main.MouseY>200){
					if(main.MouseY<250){
						if(CurrentButton!=2){
							CurrentButton=2;
							PlaySound.PlayRoll();
						}
					buttonEnabled.draw(225,200);	
					buttonDisabled.draw(225,100);
					}else{
						CurrentButton=0;
						buttonDisabled.draw(225,200);
						buttonDisabled.draw(225,100);
					}}else{
						CurrentButton=0;
					buttonDisabled.draw(225,200);
					buttonDisabled.draw(225,100);

					
					}}else{
						CurrentButton=0;
					buttonDisabled.draw(225,200);
					buttonDisabled.draw(225,100);
				}	
				}else{
					CurrentButton=0;
					buttonDisabled.draw(225,200);
					buttonDisabled.draw(225,100);
				}
		}else{
			CurrentButton=0;
			buttonDisabled.draw(225,200);
			buttonDisabled.draw(225,100);
		}
		
		//Finaly, Draw text
		//g.drawString("Play on DEV servers", 55, 106);
		fpsFont.drawString(250, 115, "SinglePlayer");
		fpsFont.drawString(257, 215, "Multiplayer");
		BigFont.drawString(58, 50, "Utopian Worlds - Developer Test");
		
	
	}

}
