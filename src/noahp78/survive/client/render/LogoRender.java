package noahp78.survive.client.render;

import noahp78.survive.client.main;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
/** Renders the "Noahp78" Logo on the screen
 * 
 * @author noahp78
 *
 */
public class LogoRender {
	private static int TotalTime = 1000;
	private static int CurrentTime = 0;
	private static Image Logo;
	/** Init method for the render screen. This loads textures and must be called BEFORE render 
	 * 
	 */
	public static void Init(){
		try {
			Logo = new Image("assets/textures/noahp78.png");
			//bulb = new Image("assets/textures/ConnectingBulb.png");
			
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	public static void render(GameContainer gc, Graphics g) throws SlickException {
		CurrentTime++;
		g.setBackground(new Color(1.0f, 1.0F, 1.0f));
		if (!(CurrentTime==TotalTime)){
			Logo.draw(150,150);
		}else{
			main.Menu=5;
		}
	}
}
