package noahp78.survive.client.sound;

import org.newdawn.slick.Sound;

/** A simple class to be able to play sounds
 * 
 * @author noah
 *
 */
public class PlaySound {
	private static Sound sound1;
	private static Sound click1;
	private static Sound click2;
	private static Sound rollover1;
	private static Sound rollover2;
	private static Sound switch1;
	private static Sound switch2;
	private static int click = 2;
	private static int roll = 2;
	public static void Init() throws Exception{
		sound1 =  new Sound("assets/sounds/intro.ogg");
		click1 = new Sound("assets/sounds/ui/click1.ogg");
		click2 = new Sound("assets/sounds/ui/click2.ogg");
		rollover1= new Sound("assets/sounds/ui/rollover1.ogg");
		rollover2= new Sound("assets/sounds/ui/rollover2.ogg");
	}
	public static void PlayMenuBackground(){
		//sound1.play(1F, 0.2F);
		sound1.loop(1F, 0.1F);
	}
	public static void PlayClick(){
		if (click==2){
			click=1;
			click1.play(1F, 0.5F);
		}else{
			click=2;
			click2.play(1F,0.5F);
			
		}
	}
	public static void PlayRoll(){
		if (roll==2){
			roll=1;
			rollover1.play(1F, 0.8F);
		}else{
			roll=2;
			rollover2.play(1F,0.8F);
		}
	}
	public static void StopBackground(){
		sound1.stop();
	}
	
	
	
}
