package noahp78.survive.client;

import org.newdawn.slick.Image;
/** Represents a client side player.
 * 
 * @author noahp78
 *
 */
public class clientSidePlayer {
	public int CharX;
	public int CharY;
	public int CharID;
	public Image playerImage;
	
}
