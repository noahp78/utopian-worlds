package noahp78.survive.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class ConfigHelper {
	public static int Port;
	public static Boolean WarnOnOverload;
	public static Boolean LogTickTime;
	public static Boolean PluginEventBroadcast;
	
	public static void MakeServerConfig(){
		Properties prop = new Properties();
		OutputStream output = null;
	 
		try {
	 
			output = new FileOutputStream("server.properties");
	 
			// set the properties
			prop.setProperty("Port", "25565");
			prop.setProperty("WorldGenerator", "UW1");
			prop.setProperty("WarnOnOverload", "true");
			prop.setProperty("LogTickTime", "true");
			prop.setProperty("PluginEvent", "true");
			// save properties to project root folder
			prop.store(output, null);
	 
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
		}
	}
	public static void LoadServerConfig(){

		Properties prop = new Properties();
		InputStream input = null;
	 
		try {
	 
			input = new FileInputStream("server.properties");
	 
			// load a properties file
			prop.load(input);
	 
			// get the property value and print it out
			System.out.println(prop.getProperty("WarnOnOverload"));
			System.out.println(prop.getProperty("LogTickTime"));
			System.out.println(prop.getProperty("PluginEvent"));
			
	 
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
