package noahp78.survive.util.research;

/** Represents a Research "Node"
 * 
 * @author noahp78
 *
 */
public class ResearchNode {
	public int ID;
	public int Cost;
	public String Name;
	public String Description;
	
}
