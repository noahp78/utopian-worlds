package noahp78.survive.util.research;

import java.util.HashMap;

/** Represents a "holder" for research Nodes
 * 
 * @author noah
 *
 */
public class Era {
	public Era before;
	public Era after;
	public HashMap<ResearchNode, Integer>Research = new HashMap<ResearchNode,Integer>();
	
}
