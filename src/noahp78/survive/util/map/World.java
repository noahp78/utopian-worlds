package noahp78.survive.util.map;

import java.util.HashMap;

import noahp78.survive.server.managers.ServerSidePlayer;
import noahp78.survive.util.map.io.Container;

/** World Class, Make your own worlds with this one!
 * 
 * 
 * 
 * @author noah
 * @param
 * @side BOTH
 */
public class World {
	//public HashMap<String, Integer>WorldMap = new HashMap<String,Integer>();
	// Add a Container
	public Container worldContainer;
	public String worldName;
	public HashMap<String,ServerSidePlayer>PlayerMaps = new HashMap<String,ServerSidePlayer>();
	
	
	
}
