package noahp78.survive.util.map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class DefaultTileSet {
	/** Grass = 1
	 *  Tree=2
	 *  Dirt=3
	 *  Sand=4
	 *  Stone=5
	 *  Water=6
	 * @throws SlickException
	 */
	public static void init() throws SlickException{
		BasicTile GrassTile = new BasicTile();
		GrassTile.Humanname="Grass";
		GrassTile.techName="game.GrassTile";
		GrassTile.tileImage = new Image("assets/textures/grassTile.png");
		TileRegistry.Register(GrassTile);
		BasicTile TreeTile = new BasicTile();
		TreeTile.Humanname="Tree";
		TreeTile.techName="game.treeTile";
		TreeTile.tileImage = new Image("assets/textures/treeTile.png");
		TreeTile.LightEmit = 10;
		TileRegistry.Register(TreeTile);
		BasicTile DirtTile = new BasicTile();
		DirtTile.Humanname="Dirt";
		DirtTile.techName="game.dirtTile";
		DirtTile.tileImage = new Image("assets/textures/DirtTile.png");
		TileRegistry.Register(DirtTile);
		BasicTile SandTile = new BasicTile();
		SandTile.Humanname="Sand";
		SandTile.techName="game.sandTile";
		SandTile.tileImage = new Image("assets/textures/SandTile.png");
		TileRegistry.Register(SandTile);
		BasicTile StoneTile = new BasicTile();
		StoneTile.Humanname="Stone";
		StoneTile.techName="game.stoneTile";
		StoneTile.tileImage = new Image("assets/textures/StoneTile.png");
		TileRegistry.Register(StoneTile);
		BasicTile WaterTile = new BasicTile();
		WaterTile.Humanname = "water";
		WaterTile.techName="game.liquidWater";
		WaterTile.tileImage = new Image("assets/textures/WaterTile.png");
		TileRegistry.Register(WaterTile);
	}
}
