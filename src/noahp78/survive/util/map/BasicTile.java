package noahp78.survive.util.map;

import org.newdawn.slick.Image;

/** Represents a basic Tile, Use this to create tiles and register them with noahp78.survive.util.map.TileRegistry.register(tile);
 * 
 * 
 * @author noah
 * HumanName: Human Readable name of your Tile.
 * techName: Name without spaces for logs and code.
 * tileImage: Image for the game to render
 * LightEmit: Light level the tile emits
 * CurrentLight: You don't need to set this. This gets overwritten!
 */
public class BasicTile {
	public String Humanname;
	public String techName;
	public Image tileImage;
	public int LightEmit;
	public Float CurrentLight;
}
