package noahp78.survive.util.map;

import org.newdawn.slick.Image;


/** Advanced Tile
 * 
 * @author noah
 *
 */
public class Tile {
	private static Image texture;
	/** Render Function
	 * 
	 */
	public static void Render(int X, int Y){}
	
	/** Change block texture
	 * 
	 */
	public static void setTexture(int X, int Y, Image Texture){
	}
	/** called when the block itself get's a random tick
	 * 
	 * @param X
	 * @param Y
	 */
	public static void OnTick(int X, int Y){}
	
	public static void OnDestory(int X, int Y){}
	/** Set what tile is dropped back to the user
	 * 
	 * @param X
	 * @param Y
	 * @param Reason
	 * @return
	 */
	public static Tile itemReturn(int X, int Y, int Reason){
		return null;
	}
	/** Should this tile recieve block updates randomly?
	 * 
	 * @param X
	 * @param Y
	 * @return
	 */
	public static Boolean shouldRandomTick(int X, int Y){
		return false;
	}
	public static Image getTexture(int X,int Y){
		return texture;
	}
	
}
