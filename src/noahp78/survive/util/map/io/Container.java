package noahp78.survive.util.map.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import noahp78.survive.server.generators.ChunkGen;
import noahp78.survive.util.logManager;

/** Represents a "Chunk Container" that holds all the chunks. Used to write and read from disk
 *  (DEV Note: This might be too big if we get lots of data)
 * @author noah
 *
 */
public class Container {
	/** Please get the Top Cordinates of this chunk
	 * 
	 */
	public static HashMap<String, Chunk> Container = new HashMap<String,Chunk>(); 
	public static HashMap<String, Integer> ContainerSafe = new HashMap<String, Integer>();
	
	/** Puts the chunk into chunkContainer for Container.WriteToDisk;
	 * 
	 * @param c - Chunk. The chunk to safe
	 * @param X - X coordinate of top left
	 * @param Y - Y coordinate of top left
	 */
	public static void WriteChunk(Chunk c, int X, int Y){
		//Write out to chunkContainer
		System.out.println("[SERVER][CONTAINER] Writing out chunk data for cords: " + X +":" +  Y);
		String Combined = (X + ":" + Y);	
		Container.put(Combined, c);		
	}
	public static Chunk LoadChunk(int X, int Y){
		if ((Container.get(X+":"+Y)!=null)){
			System.out.println("[ChunkContainer] Got call for chunk: " + X +":"+ Y);
			Chunk ChunkOut = Container.get(X+":"+Y);
			return ChunkOut;
		}else{
			//We should generate a new chunk
			ChunkGen.Generate(X, Y);
			if ((Container.get(X+":"+Y)!=null)){
				Chunk ChunkOut = Container.get(X+":"+Y);
				return ChunkOut;
			}else{
				//Strange... Chunk doesn't get generated
				logManager.LogCritWarning("Couldn't Generate world at: " + X + " : " + Y +"; Is this the end?");
				return null;
			}
		}
		
	}
	
	/** Writes out the ChunkContainer to disk
	 * @throws Exception 
	 *  
	 */
	public void WriteToDisk(String Name) throws Exception{
		FileOutputStream fout = new FileOutputStream("./world.dat");
		ObjectOutputStream oos = new ObjectOutputStream(fout);   
		oos.writeObject(this);
		oos.close();
		System.out.println("Done");
	}
	/** Loads the ChunkContainer from disk
	 * 
	 */
	public static void LoadFromDisk(String Name){
		
	}
}
