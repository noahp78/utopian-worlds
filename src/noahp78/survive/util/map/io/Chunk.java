package noahp78.survive.util.map.io;

import java.util.HashMap;

import noahp78.survive.util.logManager;

/** Represents a chunk with data. Can be stored to disk and sent to Clients
 *  16*16
 * @author noah
 *
 */
public class Chunk {
	public HashMap<String, Integer> Chunk = new HashMap<String,Integer>(); 
	/** Are functions spawned on these sides?
	 * 
	 */
	public Boolean FunctionLeft;
	public Boolean FunctionRight;
	public Boolean FunctionUp;
	public Boolean FunctionBottom;
	public void AddToMap(int X, int Y, int ID){
		Chunk.put(X+":"+Y, ID);
	}
	/** This clears the entire chunk. Making it open for GC to clear it.
	 *  (If we get memory problems use this!)
	 */
	public void UnloadChunk(){
		
	}
	public int GetFromMap(int X, int Y){
		if (Y<17){
		if (Chunk.get(X+":"+Y)!=null){
		return Chunk.get(X+":"+Y);
		}else{
			System.out.println("NULL Found for: " + X+ "  " + Y);
			return (Integer) null;

		}
		}else{
			logManager.LogCritWarning("Stupid game. You can't ask for 17!");
			return 2;
		}
		
	}
}
