package noahp78.survive.util;

public class Reference {
	private static String Version = "Alpha2 - Dev Build";
	public static String GetVersion(){
		return Version;
	}
	private static int LoadedPlugins = 0;
	public static int GetLoadedPlugins(){
		return LoadedPlugins;
	}
	public static void LoadPlugin(){
		LoadedPlugins++;
	}
	private static boolean Debug = true;
	public static boolean getDebug(){
		return Debug;
	}
	
}
