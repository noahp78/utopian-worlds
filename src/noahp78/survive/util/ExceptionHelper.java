package noahp78.survive.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
/** This handles all the crashes.
 * 
 * @author noah
 *
 */
public class ExceptionHelper implements Thread.UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread arg0, Throwable arg1) {
		logManager.LogCrash(getStackTrace(arg1));
	
		
		
	}
	  private String getStackTrace(Throwable aThrowable) {
		    final Writer result = new StringWriter();
		    final PrintWriter printWriter = new PrintWriter(result);
		    aThrowable.printStackTrace(printWriter);
		    return result.toString();
	 }

}
