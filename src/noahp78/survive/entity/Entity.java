package noahp78.survive.entity;


public interface Entity {
	/**
	 * 
	 * @return The number of ticks this entity has been alive for
	 */
	public int GetEntityLifeTime();
	/** Make this entity do it's thing. (despawning?)
	 * 
	 */
	public void DoTick();
	public int GetX();
	public int GetY();
	//public void WalkTo(int X, int Y);
	/*
	 * 	AIMap map = new AIMap();

        AStarPathFinder pathFinder = new AStarPathFinder(map, 100, false);
        Path path = pathFinder.findPath(null, this.X, this.Y, X, Y);

        int length = path.getLength();
        System.out.println("Found path of length: " + length + ".");

        for(int i = 0; i < length; i++) {
            System.out.println("Move to: " + path.getX(i) + "," + path.getY(i) + ".");
        }
	 */
	
}
