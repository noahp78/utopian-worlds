package noahp78.survive.entity.AI;

import noahp78.survive.server.serverStart;

import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

public class AIMap implements TileBasedMap {

	@Override
	public boolean blocked(PathFindingContext ctx, int x, int y) {
		// TODO Auto-generated method stub
		int chunkX = x%16;
		int chunkY = y%16;
		int Chunk_x = x - chunkX;
		int chunk_y = y - chunkY;
		int tile = serverStart.serverWorld.worldContainer.LoadChunk(Chunk_x, chunk_y).GetFromMap(chunkX, chunkY);
		
		if(tile!=1){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public float getCost(PathFindingContext arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getHeightInTiles() {
		// TODO Auto-generated method stub
		return 1500;
	}

	@Override
	public int getWidthInTiles() {
		// TODO Auto-generated method stub
		return 1500;
	}

	@Override
	public void pathFinderVisited(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
