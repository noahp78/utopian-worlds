package noahp78.survive.plugin.events;

import noahp78.survive.plugin.Event;
/** Called when Client is ready to start loading mods.
 * 
 * @author noah
 * @side CLIENT
 */
public class ClientStartEvent extends Event {}
