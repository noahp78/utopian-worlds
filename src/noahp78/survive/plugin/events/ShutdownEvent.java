package noahp78.survive.plugin.events;

import noahp78.survive.plugin.Event;

/** use this to unload your stuff. 
 * 
 * @author noah
 * @side BOTH (use event.ClientSide to see if it's client or not!)
 * 
 */
public class ShutdownEvent extends Event {
	public boolean ClientSide;
	
}
