package noahp78.survive.plugin.events;

import noahp78.survive.plugin.Event;

/** Called when the server "ticks"
 * WARNING: SERVER MIGHT TICK WHEN NOBODY IS ONLINE, CHECK IF THERE ARE PLAYERS BEFORE DOING STUFF
 *  WITH PLAYERS!
 * @author noahp78
 * @side SERVER
 */
public class TickEvent extends Event {

}
