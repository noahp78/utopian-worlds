package noahp78.survive.plugin.events;

import noahp78.survive.plugin.Event;

/** Called when Plugins are allowed to start adding research.
 * If you do it at the "ClientInitEvent" your Research might get messed up because of the
 * Original Research Events
 * 
 * @author noah
 * @category Research
 * @version 0.4  (This is not used in 0.3)
 * 
 */
public class ResearchInitEvent extends Event{

}
