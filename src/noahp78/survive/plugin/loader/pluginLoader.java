package noahp78.survive.plugin.loader;

import java.io.File;
import java.util.Collection;

import net.xeoh.plugins.base.Plugin;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.util.PluginManagerUtil;
import noahp78.survive.plugin.EventPublisher;
import noahp78.survive.plugin.events.ShutdownEvent;
import noahp78.survive.util.logManager;

public class pluginLoader {
	private static PluginManager pm;
	public static void load(){
		pm = PluginManagerFactory.createPluginManager();
		pm.addPluginsFrom(new File("plugins/").toURI());
		BasicPlugin plugin = pm.getPlugin(BasicPlugin.class);
		plugin.GetVersion();
		
	}
	/** Requests a unload event
	 * 
	 */
	public static void Unload(){
		logManager.LogInfo("[PluginSystem] shutting down... unloading plugins!");
		EventPublisher.raiseEvent(new ShutdownEvent());
		pm.shutdown();
	}
}
