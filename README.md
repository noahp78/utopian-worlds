# README #

Welcome to the Utopian Worlds Bitbucket Repo!

### Quick Access Urls ###

* Jenkins: http://builds-noahp78.rhcloud.com/
* Offical Repo: http://repository-noahp78.forge.cloudbees.com/snapshot/Survive/Survive/
* Translations: https://www.transifex.com/projects/p/utopian-worlds
* Bugtracker: https://newaurorastudios.atlassian.net
* wiki: https://newaurorastudios.atlassian.net/wiki

### How do I get set up? ###

* Get your favorite IDE
* Add the project + dependencies
* Add the following VM argument "-Djava.library.path=./lib/Natives/{osName}
* And you should be good to go. The current main class is noahp78.survive.client.main

### Contribution guidelines ###

* Follow the general style set by "noahp78"
* Use Atlassian Smart Commits: https://confluence.atlassian.com/display/AOD/Processing+JIRA+issues+with+commit+messages
* Make your commits short but clear